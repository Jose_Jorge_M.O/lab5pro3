Se utilizó el patrón MVVM para el desarrollo. En la vista principal se muestra un formulario para buscar en la base de datos, permitiendo agregar y buscar personas por correo electrónico, además de mostrar los diferentes errores que han ocurrido y ordenar la lista de personas.

En el ViewModel se encuentra la lógica donde se llama al modelo para actualizar la vista. En el modelo, se encuentra la lógica para los filtros en la lista de personas. También hay una clase donde se guardan los logs de errores en la aplicación. En caso de error, se guarda en el stackError y en la aplicación no ocurrirá nada. Con el botón de errores se pueden visualizar los errores.

