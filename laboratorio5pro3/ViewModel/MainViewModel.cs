using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using laboratorio5pro3.Model;

namespace laboratorio5pro3.ViewModel;
internal class MainViewModel : ObservableObject, IObserver
{

    private ObservableCollection<Data> dataList;
    private ObservableCollection<ErrorLog> errorLogList;
    private Data data;
    public Data Data { get { return data; } set{ SetProperty(ref data, value, nameof(Data)); } }
    public ObservableCollection<Data> DataList { get { return dataList; } set { SetProperty(ref dataList, value, nameof(DataList)); } }
    public ObservableCollection<ErrorLog> ErrorLogList { get { return errorLogList; } set { SetProperty(ref errorLogList, value, nameof(ErrorLogList)); } }
    private readonly IObserver emailSender = new EmailSender();
    private readonly DataBase dataBase = new DataBase();

    public ICommand cmdSave { get; }
    public ICommand cmdSearchEmail { get; }
    public ICommand cmdOrderByNameAscending { get; }
    public ICommand cmdOrderByNameDescending { get; }
    public ICommand cmdShowErrors { get; }
    public MainViewModel() {
        dataBase.Attach(this);
        dataBase.Attach(emailSender);
        this.Data = new Data();
        cmdSave = new Model.RelayCommand(Save);
        cmdSearchEmail = new Model.RelayCommand(SearchEmail);
        cmdOrderByNameAscending = new Model.RelayCommand(OrderByNameAscending);
        cmdOrderByNameDescending = new Model.RelayCommand(OrderByNameDescending);
        cmdShowErrors = new Model.RelayCommand(ShowErrors);
    }

    public void Update(ISubject subject)
    {
        if ((subject as DataBase).Filter) {
                DataList = new ObservableCollection<Data>((subject as DataBase).GetDataList());
        }
    }

    public void Save() {
        if (Data.CheckNull())
        {
            ErrorLog.errorStack.Push(new ErrorLog("ViwModel Data is Null", DateTime.Now));
        }
        else {
            dataBase.Save(Data);
        }
    }

    public void SearchEmail() {
        dataBase.SearchByEmail(Data.Email);
    }

    public void OrderByNameAscending() {
        dataBase.OrderByNameAscending();
    }

    public void OrderByNameDescending()
    {
       dataBase.OrderByNameDescending();
    }

    public void ShowErrors()
    {
        ErrorLogList = new ObservableCollection<ErrorLog>(new List<ErrorLog>(ErrorLog.errorStack));
    }
}
