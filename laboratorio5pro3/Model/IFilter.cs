using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio5pro3.Model;
internal interface IFilter
{
    void SearchByEmail( string email);
    void OrderByNameAscending();
    void OrderByNameDescending();
    Data? SearchByDetails(string name, string lastName, string email);

}
