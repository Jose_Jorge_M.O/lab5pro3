using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio5pro3.Model;
internal class Data
{
    public string? Name { get; set; }
    public string? Email { get; set; }
    public string? LastName {  get; set; }

    public bool CheckNull() {  return (Name == null || Email == null || LastName == null); }
}
