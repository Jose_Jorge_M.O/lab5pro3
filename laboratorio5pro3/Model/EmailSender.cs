using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio5pro3.Model;
internal class EmailSender : IObserver, IEmailSender
{
    public void SendEmail(string? email)
    {
        throw new NotImplementedException();
    }

    public void Update(ISubject subject)
    {
        if ((subject as DataBase).Send) {
            try
            {
                SendEmail(null);
            }
            catch (Exception e) { ErrorLog.errorStack.Push(new ErrorLog("emailsender error", DateTime.Now)); }
        }
        
    }
}
