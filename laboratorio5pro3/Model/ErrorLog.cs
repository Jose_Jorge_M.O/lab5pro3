using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio5pro3.Model;
internal class ErrorLog
{
    public string Message { get; set; }
    public DateTime Timestamp { get; set; }
    public static Stack<ErrorLog> errorStack = new Stack<ErrorLog>();

    public ErrorLog(string Message, DateTime time) {
        this.Message = Message;
        this.Timestamp = time;
    }
}
