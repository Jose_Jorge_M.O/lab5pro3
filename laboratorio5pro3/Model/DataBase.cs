using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio5pro3.Model;
internal class DataBase : ISubject, IFilter
{
    private bool read;
    private bool write;
    private List<IObserver> observers = new List<IObserver>();
    private List<Data> dataList = new List<Data>();
    private List<Data> result = new List<Data>();
    public bool Send = false;
    public bool Filter = false;

    public bool Read { 
        get { return read; } 
        set {
            if (value && Write)
            {
                ErrorLog.errorStack.Push(new ErrorLog("Can not write while is reading", DateTime.Now));
            }
            else { 
                read = value;
            }
        } 
    }
    public bool Write
    {
        get { return write; }
        set
        {
            if (value && Read)
            {
                ErrorLog.errorStack.Push(new ErrorLog("Can not read while is Writing", DateTime.Now));
            }
            else { 
                write = value;
            }
        }
    }

    public void Save(Data data) {
   
        if (data == null || data.CheckNull())
        {
            ErrorLog.errorStack.Push(new ErrorLog("Data is Null", DateTime.Now));
        }
        else {
            Data? datafound = SearchByDetails(data.Name, data.LastName, data.Email);
            if (datafound != null && dataList.Count > 0)
            {
                ErrorLog.errorStack.Push(new ErrorLog("Data Duplicated", DateTime.Now));
            }
            else {
                dataList.Add(new Data() { Name = data.Name, LastName = data.LastName, Email = data.Email });
                result = new List<Data>(dataList);
                Send = true;
                Filter = true;
                Notify();
                Send = false;
                Filter = false;
            }
        } 
            
    }

    public void Attach(IObserver observer)
    {
        try {
            this.observers.Add(observer);
        }
        catch (Exception ex)
        {
            ErrorLog.errorStack.Push(new ErrorLog(ex.Message, DateTime.Now));
        }
    }

    public void Detach(IObserver observer)
    {
        try
        {
            this.observers.Remove(observer);
        }
        catch (Exception ex)
        {
            ErrorLog.errorStack.Push(new ErrorLog(ex.Message, DateTime.Now));

        }
    }

    public void Notify()
    {
        foreach (var observer in observers)
        {
            observer.Update(this);
        }
    }

    public List<Data> GetDataList() {
        return result;
    }

    public void SearchByEmail(string email)
    {
        if (email == null || email == "")
        {
            ErrorLog.errorStack.Push(new ErrorLog($"shearch email is {email}", DateTime.Now));
        }
        else {
            Data? datafound = (dataList.FirstOrDefault(data => data.Email == email));
            if (datafound == null)
            {
                ErrorLog.errorStack.Push(new ErrorLog("data not found", DateTime.Now));
            }
            else {
                result = new List<Data>();
                result.Add(datafound);
                Filter = true;
                Notify();
                Filter = false;
            }
        }
    }

    public void OrderByNameAscending()
    {
        result =  dataList.OrderBy(data => data.Name).ToList();
        Filter = true;
        Notify();
        Filter = false;
    }

    public void OrderByNameDescending()
    {
        result = dataList.OrderByDescending(data => data.Name).ToList();
        Filter = true;
        Notify();
        Filter = false;
    }

    public Data? SearchByDetails(string name, string lastName, string email)
    {
        Data? datafound = dataList.FirstOrDefault(data =>
                    data.Name == name &&
                    data.LastName == lastName &&
                    data.Email == email
                );
        return datafound;
    }
}
